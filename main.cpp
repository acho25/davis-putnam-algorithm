#include <iostream>
#include <string>
#include <list>
#include <bits/stdc++.h>

using namespace std;
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::ifstream;

int changed;



std::vector<int>tmp; //vecteur qui contien les valeur sat


/*
 * fonction qui affiche le double vecteur
 */
void shoow(std::vector<vector<int>> vec) {
    cout<<"start";
    cout<<endl ;
    for (int i = 0; i < vec.size(); i++) {
        for (int j = 0; j < vec[i].size(); j++)
            cout << vec[i][j] << " ";
        cout << endl;
    }


}
/*
 * fonction pour afficher une clause
 */
void print(std::vector<int> &a) {
    std::cout << "\nThe clauses are : ";

    for (int i = 0; i < a.size(); i++)
        std::cout << a[i] << ' ';
}



/*
 * fonction pour mettre à jour le vecteur de la clause
 */
int updatevec(std::vector<vector<int>> &a, int v) {

    changed=0; //pour verifier si le vecteur à changer
    tmp.push_back(v);//on ajoute v dans la liste des valeurs sat

    auto it = a.begin(); //on parcourt chaque clause
    while (it!=a.end()){
        bool bErased = false;//cette valeur permet de savoir si on devrait avancer l'iterator it ou pas
        if (std::find(it->begin(), it->end(), v) != it->end()) { //si on trouve V
            it = a.erase(it);
            bErased = true;
            //on supprime toute la clause
            if (a.size() == 0) {//on vérifie si le problem est sat
                changed = 0; //condition pour quitter le while dans la fonction backtrack
                return 0;//on retourne 0 si le problem est sat
            }
        }
         if (std::find(it->begin(), it->end(), -v) != it->end()) {//si on trouve -v

            if (it->size() == 1 and it->front() != v) { //on vérifie si la clause a qu'une seule valeur qui 'est -v
                //exemple deux clauses 1 et -1 c'est unsat
                changed = 1;//si changed prend la valeur 1 on continue le traitement dans la fonction backtrack
                tmp.clear();
                return 3; //on retourne 3 si le problem est unsat
            } else {
                it->erase(std::remove(it->begin(), it->end(), -v), it->end());//on supprime que -v dans la clause
                changed = 1;
                if (a.size() == 0) {//verifier si le problem est sat
                    changed = 0;
                    return 0;

                }
            }
        }
        if(!bErased)//si on a pas supprimer une clause on avance l'iterator
            ++it;
    }

    return 1;//on retourne 1 si on veut continuer le traitement dans la fonction backtrack

}

int unitClause2(std::vector<vector<int>> &a) {

    changed = 0;
    vector<int>test;//ce vector va contenir toutes les clauses dont la taille = 1


    for (auto it3 = a.begin(); it3 != a.end(); ++it3) {//on parcours toutes les clauses

        if (it3->size() == 1) {//si une clause contient qu'une seule valeur
            test.push_back(it3->front());//on l'ajoute dans le vecteur test
        }
    }
    std::vector<int>::size_type size = test.size();
    for(std::vector<int>::size_type h=0;h<size;h++){//on parcours le vecteur test
        int v = test[h];
        tmp.push_back(v);//on ajoute v dans la liste des valeurs sat

        /*
         * le reste est exactement pareil à la fonction updatevec
         */

        auto it = a.begin();
        while (it!=a.end()){


            bool bErased = false;
            if (std::find(it->begin(), it->end(), v) != it->end()) {


                it = a.erase(it);


                bErased = true;
                if (a.size() == 0) {
                    changed = 0;
                    return 0;
                }
            }
            if (std::find(it->begin(), it->end(), -v) != it->end()) {

                if (it->size() == 1 and it->front() != v) {
                    changed = 1;
                    tmp.clear();
                    return 3;
                } else {
                    it->erase(std::remove(it->begin(), it->end(), -v), it->end());

                    if (it->size() == 1) {
                        test.push_back(it->front());
                        ++size;
                    }
                    changed = 1;
                    if (a.size() == 0) {
                        changed = 0;
                        return 0;

                    }
                }
            }
            if(!bErased)
                ++it;
        }
    }
    return 1;


}





int pureliteral(std::vector<vector<int>> &vec) {
    vector<int> l;// vecteur qui va prendre toutes les valeurs de tous les clauses
    for (int i = 0; i < vec.size(); i++) {
        for (int j = 0; j < vec[i].size(); j++) {
            l.push_back(vec[i][j]);//on met tous les valeurs dans un seul vecteur
        }
    }
    for (int i = 0; i < l.size(); i++) {//on parcourt le vecteur
        int a = 0;
        int b = 0;
        a = count(l.begin(), l.end(), l[i]);//on calcule le nombre occurrence de chaque valeur du vecteur l
        b = count(l.begin(), l.end(), -l[i]);
        if (b == 0) { // si b =0 ça veut dire que l[i] est pure
            tmp.push_back(l[i]);//on ajoute l[i] dans la liste des valeurs sat

            auto it = vec.begin();
            while (it!=vec.end()){//on parcourt le vecteur

                if (std::find(it->begin(), it->end(), l[i]) != it->end()) {//si on trouve l[i]

                    it=vec.erase(it);//on supprime toute la clause
                    changed = 1; //condition pour contnuer le traitement dans la fonction backtrack
                    if (vec.size() == 0) {//on vérifie si le problem est sat
                        changed=0;
                        return 0;
                    }
                } else ++it; //on avance l'iterator si on trouve pas l[i]
            }
        }
         if (a == 0) {//si a =O ça veut dire que -l[i] est pure
            tmp.push_back(-l[i]);//on ajoute -l[i] dans la liste des valeurs sat
            /*
             * meme traitement precedent
             */
             auto it2 = vec.begin();
             while (it2!=vec.end()){
                if (std::find(it2->begin(), it2->end(), -l[i]) != it2->end()) {
                    it2=vec.erase(it2);
                    changed = 1;
                    if (vec.size() == 0) {
                        changed=0;
                        return 1;
                    }
                } else ++it2;
            }
        }
    }
    return 1;// on retourne 1 pour continuer le traitement dans la fonction backtrack
}


/*
 * fonction qui retourne la valeur la plus répète dans les clauses les plus petites
 */

int moms(std::vector<vector<int>> &vec) {
    int min = 100;
    int final = 0;
    std::vector<vector<int>> minclauses;//vector qui contient les clauses avec la plus petite taille
    for (int i = 0; i < vec.size(); i++) {//on cherche la taille la plus petite de toutes les clauses
        int tmp2 = vec[i].size();
        if (min > tmp2) min = tmp2;
    }
    for (int i = 0; i < vec.size(); i++) {
        if (vec[i].size() == min) minclauses.push_back(vec[i]);//on ajoute dans minclauses tous les clauses dont la taille = min
    }
    vector<int> l;
    for (int i = 0; i < minclauses.size(); i++) {
        for (int j = 0; j < minclauses[i].size(); j++) {
            l.push_back(minclauses[i][j]);//on met tous les valeurs dans un seul vecteur d'une dimension
        }
    }
    int a = 0;
    for (int i = 0; i < l.size(); i++) {//on parcours le vecteur l

        int b = count(l.begin(), l.end(), l[i]) + count(l.begin(), l.end(), -l[i]);//on compte le nombre d'occurece de chaque valeur dans ses 2 etat positif et negative
        if (a < b) {
            a = b;
            final = l[i];//final est la valeur la plus répète
        } else if (a == b and final != l[i]) {// si on a 2 valeurs avec le meme nombre d'occurrences
            int x = 0;
            int m = 0;
            for (int f = 0; f < vec.size(); f++) {//on compte le nombre d'occurences des 2 valeurs dans tout le vecteur
                m = m + count(vec[f].begin(), vec[f].end(), final) + count(vec[f].begin(), vec[f].end(), -final);
                x = x + count(vec[f].begin(), vec[f].end(), l[i]) + count(vec[f].begin(), vec[f].end(), -l[i]);
            }
            if (x > m) final = l[i];
            else final = final;

        }
    }
    return final;//on retourne la valeur la plus répétè
}




/*
 * fonction qui retourne true si le problem est sat et false s'il est unsat
 */
bool backtrack(std::vector<vector<int>> &vec) {

    do {// tant que changed = 1 ça veut dire que tant que pureliteral ou unitclause ont changé le vecteur, on reste dans la boucle
        if(vec.size()==1)return true; // pas besoin de faire le traitement s'il y a qu'une seule valeur dans le problem
        int a = unitClause2(vec);//a va prendre soit 1 0 ou 3 si c'est 1 on ne fait rien(on continue la boucle) si c'est 0 le problem est sat si c'est 3 le problem est unsat
        if(a==3) {
            tmp.clear();//on vide tmp vu que ce n'est pas les valuer qui permet d'avoir une solution sat
            return false;
            break;
        }
        if(a==0 ){
            return true;
            break;
        }
        int b = pureliteral(vec);//meme traitement pour b
        if(b==0){

            return true;
            break;
        }

    } while (changed == 1);
    //si on quitte le while ça veut dire que pureliteral et unitclause n'ont pas trouvé une solution sat ou unsat


    int m = moms(vec);//m est la valeur qui va servir à faire notre hypotheses


            std::vector<vector<int>> tmpVec(vec);//on crée un vecteur temporaire qui va etre une copie du vecteur original
            tmpVec.clear();//o vide ce vecteur vu que c'est une fonction recursive
            copy(vec.begin(), vec.end(), back_inserter(tmpVec));
            int s = updatevec(tmpVec, m);//on met à jour le vecteur avec l'hypothèse de m
            //s prend soit 3 0 ou 1 comme a et b
            if(s==3){
                tmp.clear();
                return false;

            }
            if(s==0) return true;
            //si updatevec n'a pas retourné un résultat sat ou unssat on apple backtrack sur le nouvel vecteur modifier

            if(backtrack(tmpVec)) return true;//si backtrack retourne true le problem est sat

            else {//sinon on refait le meme traitement cette fois ci avec l'hypothèse sur -m


                tmp.clear();//on vide tmp vu qu'il contient des valeurs unsat
                tmpVec.clear();
                copy(vec.begin(), vec.end(), back_inserter(tmpVec));
                int v = updatevec(tmpVec, -m);
                if(v== 3){
                    tmp.clear();
                    return false;
                }
                if(v==0) return true;


                if(backtrack(tmpVec)) {//si le problem est sat

                    return true;
                }else { // si le problem n'est pas sat
                    tmp.clear();
                    return false;
                }
            }

}


int main() {

    std::vector<vector<int>> listOfClauses;//vecteur qui contient toutes les clauses
    std::ifstream file("../sat3.txt");

    if (file.is_open()) {

        std::string line;
        while (std::getline(file, line)) {//tant que le fichier contient des lignes
            std::vector<int> clauses;//vecteur qui represent les clauses

            std::stringstream stream(line);
            while (1) {//boucle infini
                int n;
                stream >> n;
                if (!stream)//si on arrive a la fin de la ligne o quitte la boucle
                    break;
                clauses.push_back(n);//on ajoute chaque valeur de la ligne dans clauses
            }
            listOfClauses.push_back(clauses);//on ajoute la clause dans la listes des clauses
        }
        file.close();

    }

    int z = backtrack(listOfClauses);
    if(z== false)cout<<"unsat";
    if(z== true)cout<<"sat";
    cout<<endl ;
    sort(tmp.begin(),tmp.end());//on trie tmp et on affiche les valeur sat
    print(tmp);





    return EXIT_SUCCESS;
}